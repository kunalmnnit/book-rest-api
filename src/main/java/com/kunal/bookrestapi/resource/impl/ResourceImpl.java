package com.kunal.bookrestapi.resource.impl;

import com.kunal.bookrestapi.domain.Book;
import com.kunal.bookrestapi.resource.Resource;
import com.kunal.bookrestapi.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/books")
public class ResourceImpl implements Resource<Book> {

    @Autowired
    private BookService bookService;

    @Override
    public ResponseEntity<Collection<Book>> findAll() {
        return new ResponseEntity<>(bookService.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Book> findById(Long id) {
        return new ResponseEntity<>(bookService.findById(id),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Book> save(Book book) {
        bookService.save(book);
        return new ResponseEntity<>(book,HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Book> update(Book book) {
        bookService.update(book);
        return new ResponseEntity<>(bookService.findById(book.getId()),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Book> deleteById(Long id) {
        Book book = bookService.findById(id);
        bookService.deleteById(id);
        return new ResponseEntity<>(book,HttpStatus.OK);
    }
}
