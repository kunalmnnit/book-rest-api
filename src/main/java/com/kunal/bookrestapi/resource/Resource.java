package com.kunal.bookrestapi.resource;

import com.kunal.bookrestapi.domain.Book;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

public interface Resource<T> {
    @GetMapping
    ResponseEntity<Collection<T>> findAll();

    @GetMapping("{id}")
    ResponseEntity<T> findById(@PathVariable("id") Long id);

    @PostMapping(consumes = "application/json")
    ResponseEntity<T> save(@RequestBody T t);

    @PutMapping(consumes = "application/json")
     ResponseEntity<T> update(@RequestBody T t);

    @DeleteMapping("{id}")
     ResponseEntity<T> deleteById(@PathVariable("id") Long id);

}
